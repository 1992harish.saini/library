from trytond.pool import Pool
from .library import *
#import library


def register():
    Pool.register(
        Genre,
        Editor,
        EditorGenreRelation,
        Author,
        Book,
        Exemplary,
        module='library', type_='model')
