from trytond.model import ModelSQL, ModelView, fields


__all__ = [
    'Genre',
    'Editor',
    'EditorGenreRelation',
    'Author',
    'Book',
    'Exemplary',
    ]

class Genre(ModelSQL, ModelView):
    'Genre'
    __name__ = 'library.genre'

    name = fields.char('Name', required=True)

class Editor(ModelSQL, ModelView):
    'Editor'
    __name__ = 'library.editor'

    name = fields.char('Name', required=True)
    start_date = fields.Date('Start Date', help='The date at which an editor started its activity')
    genres = fields.Many2Many('library.editor-library.genre', 'editor', 'genre', 'Genres')

class EditorGenreRelation(ModelSQL):
    'Editor - Genre Relation'
    __name__ = 'library.editor-library.genre'

    editor = fields.Many2One('library.editor', 'Editor', ondelete='CASCADE', required=True)
    genre = fields.Many2One('library.genre', 'Genre', ondelete='RESTRICT', required=True)

class Author(ModelSQL, ModelView):
    'Author'
    __name__ = 'library.author'

    name = fields.Char('Name', required=True)
    books = fields.One2Many('library.book', 'author', 'Books')
    gender = fields.Selection([('man', 'Man'), ('woman', 'Woman')], 'Gender')
    # book_count = fields.Integer('Number of Books', help='Number of books an author wrote')
    birth_date = fields.Date('Birth Date')
    death_date = fields.Date('Death Date')


class Book(ModelSQL, ModelView):
    'Book'
    __name__ = 'library.book'

    title = fields.Char('Title', required=True)
    description = fields.Char('Description')
    summary = fields.Text('Summary')
    cover = fields.Binary('Cover')
    edition_stopped = fields.Boolean('Edition stopped',
    help='If True, this book will not be printed again in this version')
    author = fields.Many2One('library.author', 'Author', ondelete='RESTRICT', required=False)
    editor = fields.Many2One('library.editor', 'Editor', ondelete='RESTRICT', required=False)
    genre = fields.Many2One('library.genre', 'Genre', ondelete='RESTRICT', required=False)
    exemplaries = fields.One2Many('library.book.exemplary', 'book', 'Exemplaries')
    page_count = fields.Integer('Page Count', help='The number of page in the book')


class Exemplary(ModelSQL, ModelView):
    'Exemplary'
    __name__ = 'library.book.exemplary'

    identifier = fields.Char('Identifier')
    acquisition_price = fields.Numeric('Acquisition Price', digits=(16, 2))
    acquisition_date = fields.Date('Acquisition Date')
    book = fields.Many2One('library.book', 'Book', ondelete='CASCADE', required=True)
    